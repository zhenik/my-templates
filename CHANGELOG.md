# Changelog

## [0.2.0]

### Added
- One more template file

## [0.1.0]

### Fixed
- Remove image

### Added
- other_build
- Initial

